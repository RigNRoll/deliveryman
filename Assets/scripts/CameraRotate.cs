﻿using UnityEngine;
using System.Collections;

public class CameraRotate : MonoBehaviour {
		[SerializeField]
	float Speed;
	public string name = "";
	// Use this for initialization
	void OnGUI()
	{
				if (PlayerPrefs.GetString ("Name") == "") {
						name = GUI.TextArea (new Rect (Screen.width / 2, 70, 50, 20), name, 25);
						if (GUI.Button (new Rect (Screen.width / 2 + 75, 70, 30, 30), "Ok")) {
								PlayerPrefs.SetString ("Name", name);
						}
				}
		}
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate(Vector3.up * Time.deltaTime * Speed);
	}
}
