﻿using UnityEngine;
using System.Collections;

public class UpgradeScript : MonoBehaviour {
	[SerializeField]
	bool Inventory;

	// Use this for initialization
	void Start () {
		if(!Inventory){
		if (PlayerPrefs.GetInt ("UseCar") == 0) {
			switch(PlayerPrefs.GetInt("Car1EngineUpgradeCount"))
			{
				case 0 : GetComponent<Car>().MaxSpeedMagnitude = 20; break;
				case 1 : GetComponent<Car>().MaxSpeedMagnitude = 15; break;
				case 2 : GetComponent<Car>().MaxSpeedMagnitude = 20; break;
				case 3 : GetComponent<Car>().MaxSpeedMagnitude = 25; break;
			}
			
			switch(PlayerPrefs.GetInt("Car1WDUpgradeCount"))
			{
				case 0 : GetComponent<Car>().WheelSpeed = 15; break;
				case 1 : GetComponent<Car>().WheelSpeed = 10; break;
				case 2 : GetComponent<Car>().WheelSpeed = 15; break;
				case 3 : GetComponent<Car>().WheelSpeed = 20; break;
			}
		}
			else if(PlayerPrefs.GetInt("UseCar") == 1){
				switch(PlayerPrefs.GetInt("Car2EngineUpgradeCount"))
				{
					case 0 : GetComponent<Car>().MaxSpeedMagnitude = 15; break;
					case 1 : GetComponent<Car>().MaxSpeedMagnitude = 20; break;
					case 2 : GetComponent<Car>().MaxSpeedMagnitude = 25; break;
					case 3 : GetComponent<Car>().MaxSpeedMagnitude = 30; break;
				}

				switch(PlayerPrefs.GetInt("Car2WDUpgradeCount"))
				{
					case 0 : GetComponent<Car>().WheelSpeed = 10; break;
					case 1 : GetComponent<Car>().WheelSpeed = 15; break;
					case 2 : GetComponent<Car>().WheelSpeed = 20; break;
					case 3 : GetComponent<Car>().WheelSpeed = 25; break;
				}
			}

			else if(PlayerPrefs.GetInt("UseCar") == 2){
				switch(PlayerPrefs.GetInt("Car3EngineUpgradeCount"))
				{
					case 0 : GetComponent<Car>().MaxSpeedMagnitude = 20; break;
					case 1 : GetComponent<Car>().MaxSpeedMagnitude = 25; break;
					case 2 : GetComponent<Car>().MaxSpeedMagnitude = 30; break;
					case 3 : GetComponent<Car>().MaxSpeedMagnitude = 35; break;
				}
				
				switch(PlayerPrefs.GetInt("Car3WDUpgradeCount"))
				{
					case 0 : GetComponent<Car>().WheelSpeed = 15; break;
					case 1 : GetComponent<Car>().WheelSpeed = 20; break;
					case 2 : GetComponent<Car>().WheelSpeed = 25; break;
					case 3 : GetComponent<Car>().WheelSpeed = 30; break;
				}
			}
		}


		else


		{
			if(PlayerPrefs.GetInt ("UseCar") == 0){

				switch(PlayerPrefs.GetInt("Car1SuspensionUpgradeCount"))
				{
					case 0 : GetComponent<InventoryScript>().MaxWeight = 400; break;
					case 1 : GetComponent<InventoryScript>().MaxWeight = 550; break;
					case 2 : GetComponent<InventoryScript>().MaxWeight = 650; break;
					case 3 : GetComponent<InventoryScript>().MaxWeight = 700; break;
				}
				
				switch(PlayerPrefs.GetInt("Car1TrunkUpgradeCount"))
				{
					case 0 : GetComponent<InventoryScript>().MaxSize = 400; break;
					case 1 : GetComponent<InventoryScript>().MaxSize = 550; break;
					case 2 : GetComponent<InventoryScript>().MaxSize = 650; break;
					case 3 : GetComponent<InventoryScript>().MaxSize = 700; break;
				}

			}

			else if(PlayerPrefs.GetInt ("UseCar") == 1){

				switch(PlayerPrefs.GetInt("Car2SuspensionUpgradeCount"))
				{
					case 0 : GetComponent<InventoryScript>().MaxWeight = 550; break;
					case 1 : GetComponent<InventoryScript>().MaxWeight = 700; break;
					case 2 : GetComponent<InventoryScript>().MaxWeight = 800; break;
					case 3 : GetComponent<InventoryScript>().MaxWeight = 850; break;
				}
				
				switch(PlayerPrefs.GetInt("Car2TrunkUpgradeCount"))
				{
					case 0 : GetComponent<InventoryScript>().MaxSize = 550; break;
					case 1 : GetComponent<InventoryScript>().MaxSize = 700; break;
					case 2 : GetComponent<InventoryScript>().MaxSize = 800; break;
					case 3 : GetComponent<InventoryScript>().MaxSize = 850; break;
				}
			}

			else if(PlayerPrefs.GetInt ("UseCar") == 2){
				
				switch(PlayerPrefs.GetInt("Car3SuspensionUpgradeCount"))
				{
					case 0 : GetComponent<InventoryScript>().MaxWeight = 700; break;
					case 1 : GetComponent<InventoryScript>().MaxWeight = 850; break;
					case 2 : GetComponent<InventoryScript>().MaxWeight = 950; break;
					case 3 : GetComponent<InventoryScript>().MaxWeight = 1000; break;
				}
				
				switch(PlayerPrefs.GetInt("Car3TrunkUpgradeCount"))
				{
					case 0 : GetComponent<InventoryScript>().MaxSize = 700; break;
					case 1 : GetComponent<InventoryScript>().MaxSize = 850; break;
					case 2 : GetComponent<InventoryScript>().MaxSize = 950; break;
					case 3 : GetComponent<InventoryScript>().MaxSize = 1000; break;
				}
			}
		}

	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
