﻿using UnityEngine;
using System.Collections;

public class ButtonOpenOptions : MonoBehaviour {
	public GameObject _openMenu;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnClick()
	{
		var menus = GameObject.FindGameObjectsWithTag ("Menu");
		foreach (GameObject go in menus)
						NGUITools.SetActive (go, false);
		NGUITools.SetActive (_openMenu, true);
	}
}
