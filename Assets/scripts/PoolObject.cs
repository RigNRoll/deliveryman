﻿using UnityEngine;
using System.Collections;

public class PoolObject : MonoBehaviour {
	[SerializeField]
	int useObject, maxObject;
	[SerializeField]
	GameObject Respaun, FreeObject , Car1,Car2,Car3;
	void Awake()
	{
		if (Application.loadedLevelName != "NetvorkScene") {
						switch (PlayerPrefs.GetInt ("UseCar")) {
						case 0:
								Car1.SetActive (true);
								break;
						case 1:
								Car2.SetActive (true);

								break;
						case 2:
								Car3.SetActive (true);
								break;
						}
				}
	}
 public	void InitSpaun()
	{
		while (useObject != maxObject) {
			RPlaces[] R = Respaun.GetComponentsInChildren<RPlaces>();
			int Rand = Random.Range(1,R.Length);
			if(R[Rand].Use)
				continue;
			R[Rand].Use = true;
			useObject ++;
			NaturalResources[] O = FreeObject.GetComponentsInChildren<NaturalResources>();
			int Rand2 = Random.Range(0,O.Length-1);
			
		
   			O[Rand2].RandomParametr(R[Rand].transform);
		}
	
	}
	void Start()
	{
		if (Application.loadedLevelName != "NetvorkScene")
						InitSpaun ();
	}
	public void Pull(Transform Tr)
	{
		if (Tr.GetComponentInParent<RPlaces> ()) {
						Tr.GetComponentInParent<RPlaces> ().Use = false;
						Tr.parent = FreeObject.transform;
						Tr.localPosition = Vector3.zero;
						StartCoroutine (Push (Random.Range (10, 15)));
						useObject--;
				}
	}
	public IEnumerator Push (float TimeWait)
	{
		yield return new WaitForSeconds(TimeWait);
		bool exit = false;
	while (!exit) {
			RPlaces[] R = Respaun.GetComponentsInChildren<RPlaces>();
			int Rand = Random.Range(1,R.Length);
			if(R[Rand].Use)
				continue;
			R[Rand].Use = true;
			useObject ++;
			NaturalResources[] O = FreeObject.GetComponentsInChildren<NaturalResources>();
			int Rand2 = Random.Range(0,O.Length);
			exit  = true;

			O[Rand2].RandomParametr(R[Rand].transform);
		}
	}

}
