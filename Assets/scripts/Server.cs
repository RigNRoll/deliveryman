﻿/// <summary>
/// Server.
/// Разработанно командой Sky Games
/// sgteam.ru
/// </summary>
using UnityEngine;
using System.Collections;
using System;

public class Server : MonoBehaviour {
	[SerializeField]
	GameObject Shop, Pool,UI, StartUI , GameUI,HostButton;
	public GameObject PlayerPrefab;	// Персонаж игрока
	public GameObject HostFlag;
	public bool connected;			// Статус подключения
	private GameObject _go;			// Объект для ссылки на игрока
	public bool _visible = false;	// Статус показа меню

	private  string typeName = "GameName";
	private  string gameName = "RoomName";
	private HostData[] hostList;
	private Rect windowRect = new Rect(Screen.width/3,Screen.height / 4, Screen.width/3, Screen.height/3);


	public float vSliderValue = 0;
	void Awake()
	{
		RefreshHostList();
	
	}
	IEnumerator InitObj(float waitTime) {
		
		yield return new WaitForSeconds(waitTime);
	//	if (!Network.isServer) {
	
								if (!GameObject.FindGameObjectWithTag ("Hosting").GetComponent<HostFlag> ().StartGame) {
										UI.SetActive(true);
										StartUI.SetActive(false);
										connected = true;
										camera.enabled = false;
										camera.gameObject.GetComponent<AudioListener> ().enabled = false;
										_go = Network.Instantiate (PlayerPrefab, Vector3.zero, transform.rotation, 1) as GameObject;
									//	Shop.GetComponent<LvlShop> ().player = _go;
								} else{
										Network.Disconnect ();
				
				}
				//		} 
				
	}
	// На каждый кадр
	private void RefreshHostList()
	{
		vSliderValue = 0;
		MasterServer.RequestHostList(typeName);
	}
	void OnMasterServerEvent(MasterServerEvent msEvent)
	{
		if (msEvent == MasterServerEvent.HostListReceived)
			hostList = MasterServer.PollHostList();
	}
	void Update () {
		if (Input.GetKeyUp (KeyCode.Escape)) {
						_visible = !_visible;
			GameUI.SetActive(_visible);
				}
	}
	void DoMyWindow(int windowID) {
		if (hostList != null)
		{
			if(hostList.Length >1)
				vSliderValue = GUI.VerticalSlider (new Rect (5, 5, 20, Screen.width/7), vSliderValue, 0, (float)hostList.Length);


			for (int i = 0; i < hostList.Length; i++)
			{
				if (GUI.Button(new Rect(25, 20 + vSliderValue *-20 +(30 * i), Screen.width/3-50, 30), hostList[i].gameName))
					JoinServer(hostList[i]);
			}
		}

		
	}
	public void CreateServer()
	{
		Network.InitializeServer (10, 25000, !Network.HavePublicAddress ());
		MasterServer.RegisterHost (typeName, gameName);
	}

	public void RefreshHost()
	{
		RefreshHostList ();
	}
public	void StartGame()
	{
		if (!GameObject.FindGameObjectWithTag ("Hosting").GetComponent<HostFlag> ().StartGame) {
						Pool.GetComponent<NetworkSpaun> ().NetworkInstance ();
						GameObject.FindGameObjectWithTag ("Hosting").GetComponent<HostFlag> ().StartGame = true;
				}
	}
	
	// На каждый кадр для прорисовки кнопок
	void OnGUI () {

				
					
		
				// Если мы на сервере
				if (!connected)  {
			gameName = GUI.TextField (new Rect(Screen.width/3 ,Screen.height / 4-50, 110,30), gameName);
						
						
						GUI.Label(new Rect(Screen.width/3 ,Screen.height / 4-20, 500,500),"Лист хостов");
						windowRect = GUI.Window (0, windowRect, DoMyWindow, "");

						

					

				}
				
		}
	public void Disconected()
	{
		Network.Disconnect (200);
		HostButton.SetActive (false);
	}
	private void JoinServer(HostData hostData)
	{
		Network.Connect(hostData);
	}

	// Вызывается когда мы подключились к серверу
	void OnConnectedToServer () {

		CreatePlayer();
	}
	
	// Когда мы создали сервер
	void OnServerInitialized () {
		HostButton.SetActive (true);
		Network.Instantiate(HostFlag, transform.position, transform.rotation, 1);
		CreatePlayer();
	}
	
	// Создание игрока
	void CreatePlayer () {
		StartCoroutine("InitObj",0.5f);
						
				

	}
	
	// При отключении от сервера
	void OnDisconnectedFromServer (NetworkDisconnection info) {
		connected = false;
		camera.enabled = true;
		camera.gameObject.GetComponent<AudioListener>().enabled = true;
		Application.LoadLevel(Application.loadedLevel);
	}
	
	// Вызывается каждый раз когда игрок отсоеденяется от сервера
	void OnPlayerDisconnected (NetworkPlayer pl) {
		Network.RemoveRPCs(pl);
		Network.DestroyPlayerObjects(pl);
	}
}
