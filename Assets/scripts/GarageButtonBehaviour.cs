﻿using UnityEngine;
using System.Collections;

public class GarageButtonBehaviour : MonoBehaviour {
	[SerializeField]
	string GarageNameButton;
	UILabel UpgradeEngineLabel,
	 		UpgradeSuspensionLabel,
			UpgradeTrunkLabel,
	 		UpgradeWDLabel,
			CostEngineLabel,
	 		CostSuspensionLabel,
			CostTrunkLabel,
	 		CostWDLabel;

	int EngineUpgradeCount = 1,
	 	SuspensionUpgradeCount = 1,
	 	TrunkUpgradeCount = 1,
	 	WDUpgradeCount = 1;
	
	string useCar;

	//[SerializeField]
	int Engine, Suspension, Trunk, WD;

	int get_price(string name, int lvl)
	{

				switch(name)
				{
					case "Car1EngineUpgradeCount" :
							{
								if(lvl ==0)
							return 2000;
							if(lvl ==1)
								return 4000;
							if(lvl ==2)
								return 8000;
								}break;
		case "Car2EngineUpgradeCount" :
		{
			if(lvl ==0)
				return 2000;
			if(lvl ==1)
				return 4000;
			if(lvl ==2)
				return 8000;
		}break;
		case "Car3EngineUpgradeCount" :
		{
			if(lvl ==0)
				return 2000;
			if(lvl ==1)
				return 4000;
			if(lvl ==2)
				return 8000;
		}break;
						case "Car1SuspensionUpgradeCount" :
					{
						if(lvl ==0)
							return 2000;
						if(lvl ==1)
							return 4000;
						if(lvl ==2)
							return 8000;
					}break;
		case "Car2SuspensionUpgradeCount" :
		{
			if(lvl ==0)
				return 2000;
			if(lvl ==1)
				return 4000;
			if(lvl ==2)
				return 8000;
		}break;
		case "Car3SuspensionUpgradeCount" :
		{
			if(lvl ==0)
				return 2000;
			if(lvl ==1)
				return 4000;
			if(lvl ==2)
				return 8000;
		}break;
					case "Car1TrunkUpgradeCount" :
					{
						if(lvl ==0)
							return 2000;
						if(lvl ==1)
							return 4000;
						if(lvl ==2)
							return 8000;
					}break;
		case "Car2TrunkUpgradeCount" :
		{
			if(lvl ==0)
				return 2000;
			if(lvl ==1)
				return 4000;
			if(lvl ==2)
				return 8000;
		}break;
		case "Car3TrunkUpgradeCount" :
		{
			if(lvl ==0)
				return 2000;
			if(lvl ==1)
				return 4000;
			if(lvl ==2)
				return 8000;
		}break;
				case "Car1WDUpgradeCount" :
				{
					if(lvl ==0)
						return 2000;
					if(lvl ==1)
						return 4000;
					if(lvl ==2)
						return 8000;
				}break;
		case "Car2WDUpgradeCount" :
		{
			if(lvl ==0)
				return 2000;
			if(lvl ==1)
				return 4000;
			if(lvl ==2)
				return 8000;
		}break;
		case "Car3WDUpgradeCount" :
		{
			if(lvl ==0)
				return 2000;
			if(lvl ==1)
				return 4000;
			if(lvl ==2)
				return 8000;
		}break;
				}

		return 0;

	}

	// Use this for initialization
	void Start () {

		UpgradeEngineLabel = GameObject.Find("_LUpgradeEngineNumber").GetComponent<UILabel>();
		CostEngineLabel = GameObject.Find("_LCostEngine").GetComponent<UILabel>();
		UpgradeSuspensionLabel = GameObject.Find("_LUpgradeSuspensionNumber").GetComponent<UILabel>();
		CostSuspensionLabel = GameObject.Find("_LCostSuspension").GetComponent<UILabel>();
		UpgradeTrunkLabel = GameObject.Find("_LUpgradeTrunkNumber").GetComponent<UILabel>();
		CostTrunkLabel = GameObject.Find("_LCostTrunk").GetComponent<UILabel>();
		UpgradeWDLabel = GameObject.Find("_LUpgrade4WDNumber").GetComponent<UILabel>();
		CostWDLabel = GameObject.Find("_LCost4WD").GetComponent<UILabel>();

		if (PlayerPrefs.GetInt ("UseCar") == 0) {
			useCar = "Car1";
		}
		else if (PlayerPrefs.GetInt ("UseCar") == 1) {
			useCar = "Car2";
		}
		else  {
			useCar = "Car3";
		}
		PlayerPrefs.SetInt ("Gold",100000);

		switch (PlayerPrefs.GetInt (useCar +"EngineUpgradeCount")) 
		{
			case 0 : 
			{
				UpgradeEngineLabel.text = "1/4";
				CostEngineLabel.text = get_price(useCar +"EngineUpgradeCount", 0).ToString();
			}
			break;

			case 1:
			{
				UpgradeEngineLabel.text = "2/4";
			CostEngineLabel.text = get_price(useCar +"EngineUpgradeCount", 1).ToString();
			}
			break;

			case 2:
			{
				UpgradeEngineLabel.text = "3/4";
			CostEngineLabel.text = get_price(useCar +"EngineUpgradeCount",2).ToString();
			}
				break;

			case 3:
			{
				UpgradeEngineLabel.text = "4/4";
				CostEngineLabel.text = "MAX LEVEL";
			}
				break;
		}

		switch (PlayerPrefs.GetInt (useCar + "SuspensionUpgradeCount")) 
		{
			case 0 : 
			{
				UpgradeSuspensionLabel.text = "1/4";
			CostSuspensionLabel.text = get_price(useCar + "SuspensionUpgradeCount",0).ToString();
			}
				break;
				
			case 1:
			{
				UpgradeSuspensionLabel.text = "2/4";
			CostSuspensionLabel.text =  get_price(useCar + "SuspensionUpgradeCount",1).ToString();
			}
				break;
				
			case 2:
			{
				UpgradeSuspensionLabel.text = "3/4";
			CostSuspensionLabel.text = get_price(useCar + "SuspensionUpgradeCount",2).ToString();
			}
				break;
				
			case 3:
			{
				UpgradeSuspensionLabel.text = "4/4";
				CostSuspensionLabel.text = "MAX LEVEL";
			}
				break;
		}

		switch (PlayerPrefs.GetInt (useCar +"TrunkUpgradeCount")) 
		{
			case 0 : 
			{
				UpgradeTrunkLabel.text = "1/4";
			CostTrunkLabel.text = get_price(useCar + "TrunkUpgradeCount",0).ToString();
			}
				break;
				
			case 1:
			{
				UpgradeTrunkLabel.text = "2/4";
			CostTrunkLabel.text =  get_price(useCar + "TrunkUpgradeCount",1).ToString();
			}
				break;
				
			case 2:
			{
				UpgradeTrunkLabel.text = "3/4";
			CostTrunkLabel.text =  get_price(useCar + "TrunkUpgradeCount",2).ToString();
			}
				break;
				
			case 3:
			{
				UpgradeTrunkLabel.text = "4/4";
				CostTrunkLabel.text = "MAX LEVEL";
			}
				break;
		}

		switch (PlayerPrefs.GetInt (useCar +"WDUpgradeCount")) 
		{
			case 0 : 
			{
				UpgradeWDLabel.text = "1/4";
			CostWDLabel.text =  get_price(useCar + "WDUpgradeCount",0).ToString();
			}
				break;
				
			case 1:
			{
				UpgradeWDLabel.text = "2/4";
			CostWDLabel.text = get_price(useCar + "WDUpgradeCount",1).ToString();
			}
				break;
				
			case 2:
			{
				UpgradeWDLabel.text = "3/4";
			CostWDLabel.text = get_price(useCar + "WDUpgradeCount",2).ToString();
			}
				break;
				
			case 3:
			{
				UpgradeWDLabel.text = "4/4";
				CostWDLabel.text = "MAX LEVEL";
			}
				break;
		}
	}

	void function( string NameUpgradeCount, int UpgradeCount, UILabel UpgradeNameLabel, string _LUpgradeNameNumber, string UpgradeLevel, UILabel CostNameLabel, string _LCostName)
	{
		//if (PlayerPrefs.GetInt (NameUpgradeCount) >= UpgradeCount) {
		if(PlayerPrefs.GetInt("Gold") >= get_price(NameUpgradeCount ,UpgradeCount-1) && UpgradeCount <4){

				int gold = PlayerPrefs.GetInt("Gold") - get_price(NameUpgradeCount ,UpgradeCount-1);
				PlayerPrefs.SetInt("Gold", gold);
				UpgradeNameLabel = GameObject.Find(_LUpgradeNameNumber).GetComponent<UILabel>();
				
				CostNameLabel = GameObject.Find(_LCostName).GetComponent<UILabel>();
				if(get_price(NameUpgradeCount ,UpgradeCount) ==0){
				CostNameLabel.text = "MAX";
				UpgradeNameLabel.text = "4/4";
				PlayerPrefs.SetInt(NameUpgradeCount, UpgradeCount);
			}
				else
			{

				CostNameLabel.text =  (get_price(NameUpgradeCount ,UpgradeCount)).ToString();
				UpgradeNameLabel.text =( UpgradeCount+1) + "/4";
				PlayerPrefs.SetInt(NameUpgradeCount, UpgradeCount);
			}
				
			}
		//}
	}


	void OnClick()
	{
		switch (GarageNameButton) 
		{
			case "UpgradeEngine" : 
			{
			//if (PlayerPrefs.GetInt ("Engine") >= UpgradeCount)
		    
			function( useCar +"EngineUpgradeCount", PlayerPrefs.GetInt (useCar +"EngineUpgradeCount")+1, UpgradeEngineLabel,"_LUpgradeEngineNumber", "2/4", CostEngineLabel,"_LCostEngine" );

		//	function(useCar + "EngineUpgradeCount", 1, 6000, UpgradeEngineLabel,"_LUpgradeEngineNumber", "3/4", CostEngineLabel,"_LCostEngine", "9000$", 2 );

		//	function( useCar +"EngineUpgradeCount", 2, 9000, UpgradeEngineLabel,"_LUpgradeEngineNumber", "4/4", CostEngineLabel,"_LCostEngine", "9000$", 3 );

			}
				break;
			case "UpgradeSuspension" :
			{
			function( useCar +"SuspensionUpgradeCount", PlayerPrefs.GetInt (useCar +"SuspensionUpgradeCount")+1, UpgradeSuspensionLabel,"_LUpgradeSuspensionNumber", "2/4", CostSuspensionLabel,"_LCostSuspension" );

			//function(useCar + "SuspensionUpgradeCount", 1, 6000, UpgradeSuspensionLabel,"_LUpgradeSuspensionNumber", "3/4", CostSuspensionLabel,"_LCostSuspension", "9000$", 2 );

			//function( useCar +"SuspensionUpgradeCount", 2, 9000, UpgradeSuspensionLabel,"_LUpgradeSuspensionNumber", "4/4", CostSuspensionLabel,"_LCostSuspension", "9000$", 3 );


			}
				break;
			case "UpgradeTrunk" :
			{
			function( useCar +"TrunkUpgradeCount", PlayerPrefs.GetInt (useCar +"TrunkUpgradeCount")+1, UpgradeTrunkLabel,"_LUpgradeTrunkNumber", "2/4", CostTrunkLabel,"_LCostTrunk" );
			//function(useCar + "TrunkUpgradeCount", 1, 6000, UpgradeTrunkLabel,"_LUpgradeTrunkNumber", "3/4", CostTrunkLabel,"_LCostTrunk", "9000$", 2 );
			//function( useCar +"TrunkUpgradeCount", 2, 9000, UpgradeTrunkLabel,"_LUpgradeTrunkNumber", "4/4", CostTrunkLabel,"_LCosttrunk", "9000$", 3 );


			}
				break;
			case "Upgrade4WD" :

			{
			function( useCar +"WDUpgradeCount", PlayerPrefs.GetInt (useCar +"WDUpgradeCount")+1, UpgradeWDLabel,"_LUpgrade4WDNumber", "2/4", CostTrunkLabel,"_LCost4WD");
			//function( useCar +"WDUpgradeCount", 1, 6000, UpgradeWDLabel,"_LUpgrade4WDNumber", "3/4", CostTrunkLabel,"_LCost4WD", "9000$", 2 );
			//function( useCar +"WDUpgradeCount", 2, 9000, UpgradeWDLabel,"_LUpgrade4WDNumber", "4/4", CostTrunkLabel,"_LCost4WD", "9000$", 3 );
			}
			break;
		}
	}

	// Update is called once per frame
	void Update () {
	
	}
}
