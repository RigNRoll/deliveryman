﻿using UnityEngine;
using System.Collections;

public class NaturalResources : MonoBehaviour {

	[SerializeField]
	int LevelPrice;
	public int weight, size;

	public void RandomParametr(Transform Parent)
	{
		switch (LevelPrice){
		case 0: 
		{
			weight = Random.Range(30,75);
			size = Random.Range(30,75);
		}
			break;
		case 1: 
		{
			weight = Random.Range(15,45);
			size = Random.Range(30,75);
		}
			break;
		case 2: 
		{
			weight = Random.Range(10,30);
			size = Random.Range(10,20);
			
		}
			break;
		
		}
		transform.parent = Parent;
		transform.localPosition = Vector3.zero;
	}
	void OnTriggerEnter(Collider Col)
	{
		if (Col.tag == "Player" && !Col.isTrigger  ) {
			if (GameObject.Find ("Inventory").GetComponent<InventoryScript> ().SizeNow + size < GameObject.Find ("Inventory").GetComponent<InventoryScript> ().MaxSize && GameObject.Find ("Inventory").GetComponent<InventoryScript> ().WeightNow + weight < GameObject.Find ("Inventory").GetComponent<InventoryScript> ().MaxWeight)
			    {
								GameObject.Find ("ObjectsPool").GetComponent<PoolObject> ().Pull (transform);
				if(Application.loadedLevelName == "NetvorkScene" && Col.GetComponent<Car>().NetworkClient)
					GameObject.Find ("Inventory").GetComponent<InventoryScript>().AddObg(size, weight, LevelPrice);
				else if(Application.loadedLevelName != "NetvorkScene" )
					GameObject.Find ("Inventory").GetComponent<InventoryScript>().AddObg(size, weight, LevelPrice);
						}
				}
	}

}
