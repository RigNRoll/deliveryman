using UnityEngine;
using System.Collections;

public class Car : MonoBehaviour {
public bool NetworkClient;	
public GameObject WheelRight,
				  WheelLeft,
 				  WheelDRight,
 				  WheelDLeft;

public bool  BackWheels;
	[SerializeField]
	   bool  Fly;
	[SerializeField]
public float WheelSpeed,
			 BrakeTorque =40,
			 MaxSpeedMagnitude  = 20,
	         DriftKoef = 1;

public  float  WheelRotateAngle= 0.2f;
[SerializeField]
private	Transform centre,
		 	 	  DriftCentre;

public GameObject []wheels;

public	ParticleSystem AnimSmoke;
public	bool BoolBreak;

	public GameObject BoxConteiner;

	void OnTriggerEnter(Collider C)
	{
		if (C.tag == "Earth")
				Fly = false;
	}
	void OnTriggerExit(Collider C)
	{
		if (C.tag == "Earth")
			Fly = true;
	}
	void Start()
	{
		if (Application.loadedLevelName != "NetvorkScene" && gameObject.activeSelf) {
						GameObject.Find ("Inventory").GetComponent<InventoryScript> ().GetBox (BoxConteiner.GetComponentsInChildren<Transform> ());
				}
		}
	void Awake () {
		gameObject.GetComponent<Rigidbody>().centerOfMass = centre.localPosition;

		AnimSmoke.Stop (true);
	}
	float ControllHor()
	{		
#if UNITY_ANDROID
		if(Input.acceleration.x >0.2 )
			return 1;
		  else if(Input.acceleration.x<-0.2)
			return -1;
		else return 0;
		#else
		return Input.GetAxis("Horizontal");
		#endif

	}
float ControllVert()
	{
		#if UNITY_ANDROID
		if(Input.acceleration.y>-0.2)
			return 1;
			else if(Input.acceleration.y<-0.6)
			return -1;
		else return 0;
		#else
		return Input.GetAxis("Vertical");
		#endif
		
	}
	public 	void ButtonControll()
	{
		if (Input.GetKey (KeyCode.Space) || Input.touchCount==1) {
						BoolBreak = true;		
				} else
						BoolBreak = false;

		if (rigidbody.velocity.magnitude < MaxSpeedMagnitude) {

						if (ControllVert() > 0 && !BoolBreak) { 
								WheelDRight.GetComponent<WheelCollider> ().motorTorque = WheelSpeed;
								WheelDLeft.GetComponent<WheelCollider> ().motorTorque = WheelSpeed;
						}
						if (ControllVert() < 0) { 
								WheelDRight.GetComponent<WheelCollider> ().motorTorque = -WheelSpeed;
								WheelDLeft.GetComponent<WheelCollider> ().motorTorque = -WheelSpeed;
						}
				} else {
						WheelDRight.GetComponent<WheelCollider> ().motorTorque = 0;
						WheelDLeft.GetComponent<WheelCollider> ().motorTorque = 0;
				}
		if ( ControllVert() == 0) { 
			WheelDRight.GetComponent<WheelCollider>().motorTorque = 0;
			WheelDLeft.GetComponent<WheelCollider>().motorTorque = 0;
		}

		if ( ControllHor() <0) { 
			if(BackWheels){
				WheelRight.transform.localRotation = Quaternion.Slerp(WheelRight.transform.localRotation, Quaternion.Euler(0,-WheelRotateAngle,0),Time.deltaTime*2);
				WheelLeft.transform.localRotation = Quaternion.Slerp(WheelLeft.transform.localRotation, Quaternion.Euler(0,-WheelRotateAngle,0),Time.deltaTime*2);
			}else{
				WheelRight.transform.localRotation = Quaternion.Slerp(WheelRight.transform.localRotation, Quaternion.Euler(0,WheelRotateAngle,0),Time.deltaTime*2);
				WheelLeft.transform.localRotation = Quaternion.Slerp(WheelLeft.transform.localRotation, Quaternion.Euler(0,WheelRotateAngle,0),Time.deltaTime*2);
			}
		}
		if ( ControllHor()>0) { 
			if(BackWheels){
				WheelRight.transform.localRotation = Quaternion.Slerp(WheelRight.transform.localRotation, Quaternion.Euler(0,WheelRotateAngle,0),Time.deltaTime*2);
				WheelLeft.transform.localRotation = Quaternion.Slerp(WheelLeft.transform.localRotation, Quaternion.Euler(0,WheelRotateAngle,0),Time.deltaTime*2);
			}else{
				WheelRight.transform.localRotation = Quaternion.Slerp(WheelRight.transform.localRotation, Quaternion.Euler(0,-WheelRotateAngle,0),Time.deltaTime*2);
				WheelLeft.transform.localRotation = Quaternion.Slerp(WheelLeft.transform.localRotation, Quaternion.Euler(0,-WheelRotateAngle,0),Time.deltaTime*2);
			}
		}
		if ( ControllHor() == 0) { 

			WheelRight.transform.localRotation = Quaternion.Slerp(WheelRight.transform.localRotation, Quaternion.Euler(0,0,0),0.5f);
			WheelLeft.transform.localRotation = Quaternion.Slerp(WheelLeft.transform.localRotation, Quaternion.Euler(0,0,0),0.5f);

		
		}
	
		

		
		if (BoolBreak) { 
			WheelDRight.GetComponent<WheelCollider>().brakeTorque = BrakeTorque;
			WheelDLeft.GetComponent<WheelCollider>().brakeTorque = BrakeTorque;
			
		}
		if (BoolBreak) { 
			if (ControllHor()>0&& !Fly)
				rigidbody.AddForceAtPosition(Vector3.left*rigidbody.velocity.magnitude*DriftKoef,DriftCentre.position);
			if (ControllHor()< 0 && !Fly)
				rigidbody.AddForceAtPosition(Vector3.right*rigidbody.velocity.magnitude*DriftKoef,DriftCentre.position);
			WheelDRight.GetComponent<WheelCollider> ().motorTorque = 0;
			WheelDLeft.GetComponent<WheelCollider> ().motorTorque = 0;
		}
		if (!BoolBreak) { 

			WheelDRight.GetComponent<WheelCollider>().brakeTorque = 0;
			WheelDLeft.GetComponent<WheelCollider>().brakeTorque = 0;
		}

	}
	public void AnimationControll()
	{
				if (rigidbody.velocity.magnitude > 0.5) {
						foreach (GameObject G in wheels)
								G.transform.Rotate (Vector3.right * Time.deltaTime * rigidbody.velocity.magnitude * 50, Space.Self);
				}

		if (BoolBreak && rigidbody.velocity.magnitude > 10 && !Fly && !AnimSmoke.IsAlive()) 
				AnimSmoke.Play(true);
	
		if (BoolBreak || rigidbody.velocity.magnitude < 10 || Fly )
				AnimSmoke.Stop (true);

	}
	public void Refresh()
	{
		if (Application.loadedLevelName != "NetvorkScene") {
						transform.rotation = new Quaternion ();
						transform.Translate (0, 3, 0);
				} else if (NetworkClient) {
			transform.rotation = new Quaternion ();
			transform.Translate (0, 3, 0);
				}
	}
	void  Update (){
		if (Application.loadedLevelName != "NetvorkScene") {		
						ButtonControll ();
						AnimationControll ();
				} 


	
}
}