﻿using UnityEngine;
using System.Collections;

public class StartGameBehaviourScript : MonoBehaviour {
	[SerializeField]
	string NameButton;
	[SerializeField]
	GameObject StartMenu,LevelMenu, OptionMenu, ShopMenu;
	// Use this for initialization
	UILabel UpgradeEngineLabel,
			UpgradeSuspensionLabel,
			UpgradeTrunkLabel,
			UpgradeWDLabel;

	UILabel CostEngineLabel,
			CostSuspensionLabel,
			CostTrunkLabel,
			CostWDLabel;

	void Start () {
		//if (GUI.Button(_BPlay, "PLAY"))
		//Application.LoadLevel(TEstCar);
		QualitySettings.SetQualityLevel(5);
	}

	void OnClick()
	{
				//if(NameButton=="Play")
				//Application.LoadLevel("TEstCar");

		switch (NameButton) 
		{
		case "Play" : {	StartMenu.SetActive(false);
			LevelMenu.SetActive(true);} break;
		case "Garage" : Application.LoadLevel("Garage"); break;
		case "FirstLevel" : Application.LoadLevel("TEstCar"); break;
		case "BackFromLevel" : {StartMenu.SetActive(true);
								LevelMenu.SetActive(false);} break;
		case "Network" : Application.LoadLevel("NetvorkScene"); break;
		case "BackToMainMenu" : Application.LoadLevel("MainMenu"); break;
		case "ResetStats" : {
			//PlayerPrefs.SetInt("Gold", 0);
			PlayerPrefs.DeleteAll();

			UpgradeEngineLabel = GameObject.Find("_LUpgradeEngineNumber").GetComponent<UILabel>();
			CostEngineLabel = GameObject.Find("_LCostEngine").GetComponent<UILabel>();
			UpgradeSuspensionLabel = GameObject.Find("_LUpgradeSuspensionNumber").GetComponent<UILabel>();
			CostSuspensionLabel = GameObject.Find("_LCostSuspension").GetComponent<UILabel>();
			UpgradeTrunkLabel = GameObject.Find("_LUpgradeTrunkNumber").GetComponent<UILabel>();
			CostTrunkLabel = GameObject.Find("_LCostTrunk").GetComponent<UILabel>();
			UpgradeWDLabel = GameObject.Find("_LUpgrade4WDNumber").GetComponent<UILabel>();
			CostWDLabel = GameObject.Find("_LCost4WD").GetComponent<UILabel>();

			UpgradeSuspensionLabel.text = "1/4";
			UpgradeEngineLabel.text = "1/4";
			UpgradeTrunkLabel.text = "1/4";
			CostEngineLabel.text = "40$";
			CostSuspensionLabel.text = "4000$";
			CostTrunkLabel.text = "4000$";
			UpgradeWDLabel.text = "1/4";
			CostWDLabel.text = "4000$";
		} break;
		case "Shop" : {
			StartMenu.SetActive(false);
			ShopMenu.SetActive(true);
			} break;
		case "Options" : 
		{
			StartMenu.SetActive(false);
			OptionMenu.SetActive(true);
		}
			break;

		case "Low" : 
		{
			QualitySettings.SetQualityLevel(1);
		}
			break;

		case "Medium" : 
		{
			QualitySettings.SetQualityLevel(3);
		}
			break;

		case "High" : 
		{
			QualitySettings.SetQualityLevel(5);
		}
			break;

		case "Back" : 
		{
			StartMenu.SetActive(true);
			OptionMenu.SetActive(false);
			ShopMenu.SetActive(false);
		}
			break;
		case "Exit" : Application.Quit(); break;
		}
	
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
