﻿using UnityEngine;
using System.Collections;

public class Shop : MonoBehaviour {
	[SerializeField]
	GameObject BayCar2,UseCar2,BayCar3,UseCar3;
	[SerializeField]
	string Button;
	// Use this for initialization
	void Start () {
		if (gameObject.name == "_PShop") {
						if (PlayerPrefs.GetInt ("Car2") == 0) {
								UseCar2.SetActive (false);
								BayCar2.SetActive (true);
						} else {
								UseCar2.SetActive (true);
								BayCar2.SetActive (false);
						}
						if (PlayerPrefs.GetInt ("Car3") == 0) {
								UseCar3.SetActive (false);
								BayCar3.SetActive (true);
						} else {
								UseCar3.SetActive (true);
								BayCar3.SetActive (false);
						}
				}
	
	}
	void OnClick()
	{
		switch (Button) {
				case "UseCar1":
				PlayerPrefs.SetInt("UseCar",0);
						break;
				case "UseCar2":
					PlayerPrefs.SetInt("UseCar",1);
					break;
				case "UseCar3":
					PlayerPrefs.SetInt("UseCar",2);
					break;
				case "BayCar2":
			      if(PlayerPrefs.GetInt("Gold") >= 20000)
				{
					int G = PlayerPrefs.GetInt("Gold")-20000;
					PlayerPrefs.SetInt("Gold",G);
					UseCar2.SetActive(true);
					BayCar2.SetActive(false);
				PlayerPrefs.SetInt("Car2",1);
			}
					break;

				case "BayCar3":
			if(PlayerPrefs.GetInt("Gold") >= 20000)
			{
					int G = PlayerPrefs.GetInt("Gold")-70000;
					PlayerPrefs.SetInt("Gold",G);
					UseCar3.SetActive(true);
					BayCar3.SetActive(false);
					PlayerPrefs.SetInt("Car3",1);
			}
					break;

				}
	}
	

}
