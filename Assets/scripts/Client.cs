﻿/// <summary>
/// Client v2.
/// Разработанно командой Sky Games
/// sgteam.ru
/// </summary>
using UnityEngine;
using System.Collections;
using System;
using System.Text;
public class Client: MonoBehaviour {
	
	public Camera cam, cam1;				// ссылка на нашу камеру
	public string name;
	public int Gold;
	private float lastSynchronizationTime = 0f;
	private float syncDelay = 0f;
	private float syncTime = 0f;
	private Vector3 syncStartPosition = Vector3.zero;
	private Vector3 syncEndPosition = Vector3.zero;
	private Quaternion syncStartRotation;
	private Quaternion syncEndRotation ;	
	private Quaternion syncStartWheelsRotation;
	private Quaternion syncEndWheelsRotation ;// поворот 
	private int numCurAnim;					// номер анимации для сереализации 0 ожидание 1 ходьба 2 бег 3 прыжок 

	// на каждый кадр
	void Update () {
		if(networkView.isMine) {
			if(!cam.enabled) { 
				GameObject.FindGameObjectWithTag("Shop").GetComponent<LvlShop>().player = gameObject;
				Debug.Log(	GameObject.FindGameObjectWithTag("Shop").GetComponent<LvlShop>().player );
				cam.transform.parent = null;
				cam.enabled = true; 
				cam1.enabled = true; 
				GetComponent<Car>().NetworkClient = true;
				GameObject.Find ("Inventory").GetComponent<InventoryScript>().GetBox(GetComponent<Car>().BoxConteiner.GetComponentsInChildren<Transform>());
				string nickNames = null;
				for (int i = 0; i < name.Length; i++) {
					if ( i < name.Length - 1 )
						nickNames += name[i] + "&";
					else
						nickNames += name[i];
				}
			}
			if(	GetComponent<Car>().enabled){
			
			GetComponent<Car>().ButtonControll();
			GetComponent<Car>().AnimationControll();
			
			}
				
        
		} else {
			if(cam.enabled) { 
			
				cam.enabled = false; 
				cam1.enabled = false; 
			}
			SyncedMovement();
		}
	}

		void Start()
	{
		if(networkView.isMine)
			networkView.RPC("GetName", RPCMode.AllBuffered,  PlayerPrefs.GetString("Name") );

	}
	public void GetGold()
	{ int g = GameObject.Find ("Inventory").GetComponent<InventoryScript> ().Statistic;
		if(networkView.isMine)
			networkView.RPC("GetGoldStat", RPCMode.AllBuffered, g);

	}



	[RPC] void GetGoldStat( int gl) {
		Gold = gl;
	}


	[RPC] void GetName( string names) {
		name = names;
	}
	//на остальных клиентах



	// Вызывается с определенной частотой. Отвечает за сереализицию переменных
	void OnSerializeNetworkView (BitStream stream, NetworkMessageInfo info) {
    	Vector3 syncPosition = Vector3.zero;
		Quaternion syncRotation = new Quaternion();
		Quaternion syncRotationWheels = new Quaternion();
		bool animsmoke = false ;
		char []names= new char[0];
		int gl=0;
		int Length = 0;
	    if (stream.isWriting) {

			animsmoke = GetComponent<Car>().AnimSmoke.IsAlive();
			syncRotation = transform.rotation;
        	syncPosition = transform.position;
			syncRotationWheels = GetComponent<Car>().WheelLeft.transform.localRotation;
        	stream.Serialize(ref syncPosition);
			stream.Serialize(ref syncRotation);
			stream.Serialize(ref syncRotationWheels);
			stream.Serialize(ref animsmoke);

		
    	} else {

		
        	stream.Serialize(ref syncPosition);
			stream.Serialize(ref syncRotation);
			stream.Serialize(ref syncRotationWheels);
			stream.Serialize(ref animsmoke);


		
			// Расчеты для интерполяции
			
			// Находим время между текущим моментом и последней интерполяцией
			if(animsmoke)
			GetComponent<Car>().AnimSmoke.Play();
			else
			GetComponent<Car>().AnimSmoke.Stop();

        	syncTime = 0f;
        	syncDelay = Time.time - lastSynchronizationTime;
        	lastSynchronizationTime = Time.time;
 			

        	syncStartPosition = transform.position;
        	syncEndPosition = syncPosition;
			syncStartRotation = transform.rotation;
			syncEndRotation = syncRotation;
			syncStartWheelsRotation =  GetComponent<Car>().WheelLeft.transform.localRotation;
			syncEndWheelsRotation = syncRotationWheels;

    	}

	}
	
	// Интерполяция
	private void SyncedMovement() {
    	syncTime += Time.deltaTime;
		if (syncDelay != 0 && syncTime !=0) {
						transform.position = Vector3.Lerp (syncStartPosition, syncEndPosition, syncTime / syncDelay);
						transform.rotation = Quaternion.Lerp (syncStartRotation, syncEndRotation, syncTime / syncDelay);
						GetComponent<Car>().WheelLeft.transform.localRotation = Quaternion.Lerp (syncStartWheelsRotation, syncEndWheelsRotation, syncTime / syncDelay);
						GetComponent<Car>().WheelRight.transform.localRotation = Quaternion.Lerp (syncStartWheelsRotation, syncEndWheelsRotation, syncTime / syncDelay);

		}
	}
	



}
