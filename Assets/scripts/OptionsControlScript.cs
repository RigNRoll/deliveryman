﻿using UnityEngine;
using System.Collections;

public class OptionsControlScript : MonoBehaviour {
	[SerializeField]
	string NameButton;
	// Use this for initialization
	void Start () {
		if (NameButton == "Slider")
						GetComponent<UISlider>().value = AudioListener.volume;
	}
	
	// Update is called once per frame
	void Update () {
		if(NameButton == "Slider")
			AudioListener.volume = GetComponent<UISlider>().value;
	
	}
}
