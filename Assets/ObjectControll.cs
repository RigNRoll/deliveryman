﻿using UnityEngine;
using System.Collections;

public class ObjectControll : MonoBehaviour {

	public int _size, _weight, _type;
	[SerializeField]
	UILabel  Size , Weight;
	public void InitObj(int size, int weight, int TypeObject)
	{
		string nameObj = "";

			switch(TypeObject)
			{
			case 0 : nameObj = "Stone" ;
				break;
			case 1 : nameObj = "Tree" ;
				break;
			case 2 : nameObj = "Fruits" ;
				break;
			}
				
						
		GetComponent<UISprite>().spriteName = nameObj;
		Size.text = size.ToString ();

		Weight.text = weight.ToString ();

		_size = size;
		_weight = weight;
		_type = TypeObject;
	}
}
