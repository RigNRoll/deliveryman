﻿using UnityEngine;
using System.Collections;

public class NetworkSpaun : MonoBehaviour {
	[SerializeField]
	GameObject []Types;
	[SerializeField]
	int Count;
	// Use this for initialization
	public void NetworkInstance()
	{
		foreach (GameObject G in Types) {
			for(int i = 0 ;i < Count ; i++)
			{
				GameObject go =Network.Instantiate(G, Vector3.zero, new Quaternion(),1) as GameObject;
				go.transform.parent = transform;
				go.transform.localPosition = Vector3.zero;
			}
		}
		GameObject.Find ("ObjectsPool").GetComponent<PoolObject> ().InitSpaun ();
	}
}
