﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class InventoryScript : MonoBehaviour {

	public int CountObj;
	public bool OnOFF;
	public int MaxSize, MaxWeight, SizeNow, WeightNow,Statistic;
	[SerializeField]
	InventoryObject []Obj = new InventoryObject[0];
	[SerializeField]
	GameObject Obg;
	[SerializeField]
	UILabel CurrentSizeValueLabel, CurrentWeightValueLabel, CurrentGoldValueLabel , CurrentTimer;
	private Rect windowRect = new Rect(Screen.width/2-Screen.width/6 ,Screen.height / 2 -Screen.height/6, Screen.width/3, Screen.height/3);
	bool finish,PushStat;
	public float timer;
	[SerializeField]
	GameObject [] Box;
	float vSliderValue;
	int [] gold = new int[0] ;
	string [] name = new string[0] ;
	int myGold;
	string MyName;
	bool win;


	public IEnumerator SortStat (float TimeWait)
	{
		yield return new WaitForSeconds(TimeWait);

		GameObject []G = GameObject.FindGameObjectsWithTag("Player");
		gold = new int[G.Length] ;
		name = new string[G.Length] ;
		
		for (int i = 0; i < G.Length; i++)
		{
			if(!G[i].GetComponent<Car>().NetworkClient)
			{
			name[i] = G[i].GetComponent<Client>().name;
			gold[i] = G[i].GetComponent<Client>().Gold;
			}
			else{
				name[i] = "This You " + G[i].GetComponent<Client>().name;
				gold[i] = G[i].GetComponent<Client>().Gold;
				myGold = G[i].GetComponent<Client>().Gold;
				MyName = "This You " + G[i].GetComponent<Client>().name;
			}
		}
		for(int i=0;i <gold.Length;i++)
			for(int j=0;j <gold.Length;j++)
		{
			if(gold[i]>gold[j])
			{
				int std;
				string stds;
				std = gold[i];
				stds = name[i];
				gold[i] = gold[j];
				name[i] = name[j];
				gold[j] = std;
				name[j]= stds;
			}
			
		}
		if (gold [0] == myGold && name [0] == MyName) {
			win = true;
				}
		PushStat = true;

	}
	void DoMyWindow(int windowID) {
		if (Application.loadedLevelName != "NetvorkScene") {
			 
						GUI.Label (new Rect (Screen.width / 6 - 100, Screen.height / 6 - 25, 400, 50), "You have earned :  " + Statistic.ToString ());


				} else 
				{
		

			if(gold.Length> 3)
				vSliderValue = GUI.VerticalSlider (new Rect (5, 5, 20, Screen.width/7), vSliderValue, 0,gold.Length);

			for (int i = 0; i < gold.Length; i++)
			{

						GUI.Label(new Rect(25, 20 + vSliderValue*-20 +(30 * i), Screen.width/3-50, 30), name[i]  + "-"+ gold[i].ToString());

			}


				}

		
	}

	void OnGUI()
	{
		if (PushStat) {
			GUI.Label(new Rect(Screen.width/2-30,Screen.height / 2 -Screen.height/6-60, Screen.width/3, Screen.height/3),"Statistic");
			if(win)
				GUI.Label(new Rect(Screen.width/2-30,Screen.height / 2 -Screen.height/6-30, Screen.width/3, Screen.height/3),"You WIN");
			else
				GUI.Label(new Rect(Screen.width/2-30,Screen.height / 2 -Screen.height/6-30, Screen.width/3, Screen.height/3),"Loser");

			windowRect = GUI.Window (0, windowRect, DoMyWindow, "");
				}
	}
	void Update()
	{
		if (timer > 0) {
			if (Application.loadedLevelName != "NetvorkScene")
				timer -= Time.deltaTime;
			else if (GameObject.FindWithTag ("Hosting")) {
				if (GameObject.FindWithTag ("Hosting").GetComponent<HostFlag> ().StartGame) {
					timer -= Time.deltaTime;
				}
			}
		}
		else {
			timer -= Time.deltaTime;

			if(!finish){
			finish = true;
			
			StartCoroutine( "SortStat",5);
			foreach(GameObject G in GameObject.FindGameObjectsWithTag("Player"))
			{		
					G.GetComponent<Client>().GetGold();
					G.GetComponent<Car>().enabled = false;
					G.GetComponent<Car>().WheelDLeft.GetComponent<WheelCollider> ().motorTorque = 0;
					G.GetComponent<Car>().WheelDRight.GetComponent<WheelCollider> ().motorTorque = 0;
				}

			
			}
		}
		if(timer>=0)
		CurrentTimer.text = ((int)timer).ToString ();
	}
	public void GetBox (Transform[] B)
	{
		Box = new GameObject[(B.Length - 1)/3];
		int i = 0;
		foreach (Transform G in B) {
		if(G.GetComponent<Box>()){
				Box[i]=G.gameObject;
				i++;}
		}
		UpdateBox ();
	}
	void UpdateBox()
	{

		float pr = (((float)SizeNow/(float)MaxSize) *10) +1;
	
		foreach (GameObject G in Box) {
				if(G.GetComponent<Box>().Numer < pr)
					G.SetActive(true);
				else
					G.SetActive(false);

		
				}
	}
	void UpdateValue()
	{
		CurrentSizeValueLabel.text = SizeNow.ToString () + "/" + MaxSize.ToString ();
		CurrentWeightValueLabel.text = WeightNow.ToString () + "/" + MaxWeight.ToString ();
		CurrentGoldValueLabel.text = PlayerPrefs.GetInt ("Gold").ToString ();
	
		}

	void Start()
	{
		CurrentTimer = GameObject.Find ("_LCurrentTimerValue").GetComponent<UILabel> ();
		CurrentSizeValueLabel = GameObject.Find ("_LCurrentSizeValue").GetComponent<UILabel> ();
		CurrentWeightValueLabel = GameObject.Find ("_LCurrentWeightValue").GetComponent<UILabel> ();
		CurrentGoldValueLabel = GameObject.Find ("_LCurrentGoldValue").GetComponent<UILabel> ();
		UpdateValue ();
		}

	public void AddObg(int s,int w, int t)
	{

						SizeNow = SizeNow + s;
						WeightNow = WeightNow + w;
						UpdateValue ();
						GameObject G = Instantiate (Obg) as GameObject;
						G.transform.parent = transform;
						G.GetComponent<ObjectControll> ().InitObj (s, w, t);
						
						CountObj++;

						InventoryObject [] std = new InventoryObject[Obj.Length];

						int i = 0;
						foreach (InventoryObject I in Obj) {
								std [i] = I;
								i++;
						}
						Obj = new InventoryObject[CountObj];
						i = 0;
						foreach (InventoryObject I in std) {
								Obj [i] = I;
								i++;
						}
						Obj [CountObj - 1] = new InventoryObject (s, w, t);
				if (!OnOFF)
						G.transform.localPosition = new Vector3 (-200, 0, 0);
				else
						OnInventar ();
		UpdateBox ();

	}
	public void OnInventar()
	{
		int i = 0;
		foreach (ObjectControll G in GetComponentsInChildren<ObjectControll>()) {
						G.transform.localPosition = new Vector3 (i*2, 0, 0);
			i++;
				}
		OnOFF = true;
		transform.localPosition = Vector3.zero;
		GetComponent<Slider> ().std = Vector3.zero;
	}
	public void OffInventar()
	{
		foreach (ObjectControll G in GetComponentsInChildren<ObjectControll>())
			G.transform.localPosition = new Vector3 (-200, 0, 0);
		OnOFF = false;
	}

	public void Drop(Transform T)
	{
				SizeNow -= T.GetComponent<ObjectControll> ()._size;
				WeightNow -= T.GetComponent<ObjectControll> ()._weight;
				UpdateValue ();
		UpdateBox ();
		InventoryObject [] std = new InventoryObject[CountObj-1];
		for (int i = 0, j=0; i < CountObj; i++)
		{
			if(Obj[i].Weight == T.GetComponent<ObjectControll> ()._weight  && Obj[i].Size == T.GetComponent<ObjectControll> ()._size && Obj[i].Type == T.GetComponent<ObjectControll> ()._type)
				continue;
			else{
				std[j] = Obj[i];
				j++;
			}
		}

				Destroy (T.gameObject);
				CountObj--;
		Obj = new InventoryObject[CountObj];
		int k = 0;
		foreach (InventoryObject I in std) {
			Obj [k] = I;
			k++;
		}
				
				
	}
	public void Sell()
	{

		foreach (InventoryObject I in Obj) {
			switch (I.Type){
			case 0: PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold") +  1*I.Weight); Statistic += 1*I.Weight; break;
			case 1: PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold") +  2*I.Weight);Statistic += 2*I.Weight; break;
			case 2: PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold") +  5*I.Weight);Statistic += 5*I.Weight; break;
			default :break;
			}

		}
		Debug.Log (PlayerPrefs.GetInt ("Gold"));
		Obj = new InventoryObject[0];
		foreach (Transform G in GetComponentsInChildren<Transform>())
				if(G.gameObject != gameObject)
						Destroy (G.gameObject);
		CountObj = 0;
		WeightNow = 0;
		SizeNow = 0;
		UpdateValue ();
		UpdateBox ();
	}
}
