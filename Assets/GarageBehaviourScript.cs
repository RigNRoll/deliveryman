﻿using UnityEngine;
using System.Collections;

public class GarageBehaviourScript : MonoBehaviour {
	[SerializeField]
	GameObject UseCar1, UseCar2, UseCar3;
	//[SerializeField]
	//string Button;

	// Use this for initialization
	void Start () {
		QualitySettings.SetQualityLevel(5);

		if (PlayerPrefs.GetInt ("UseCar") == 0) {
						UseCar1.SetActive (true);
						UseCar2.SetActive (false);
						UseCar3.SetActive (false);
				}
		if (PlayerPrefs.GetInt ("UseCar") == 1) {
			UseCar1.SetActive (false);
			UseCar2.SetActive (true);
			UseCar3.SetActive (false);
		}
		if (PlayerPrefs.GetInt ("UseCar") == 2) {
			UseCar1.SetActive (false);
			UseCar2.SetActive (true);
			UseCar3.SetActive (false);
		}
				
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
