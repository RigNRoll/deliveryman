﻿using UnityEngine;
using System.Collections;
[SerializeField]
public class InventoryObject : MonoBehaviour {

	public int Size, Weight, Type;
	public	InventoryObject ()
	{
		Size = 0;
		Weight = 0;
		Type = 0;
	}
public	InventoryObject ( int s,int w,int t)
	{
		Size = s;
		Weight = w;
		Type = t;
	}

}
