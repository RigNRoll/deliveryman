﻿using UnityEngine;
using System.Collections;

public class ObjClient : MonoBehaviour {


	
	void OnSerializeNetworkView (BitStream stream, NetworkMessageInfo info) {
		Vector3 syncPosition = Vector3.zero;
		int size =0, weight=0;
		bool st = false;
		if (transform.tag != "Hosting") {
						if (stream.isWriting) {

								syncPosition = transform.position;
								size = GetComponent<NaturalResources> ().size;
								weight = GetComponent<NaturalResources> ().weight;
								stream.Serialize (ref syncPosition);
								stream.Serialize (ref size);
								stream.Serialize (ref weight);
			
						} else {
								stream.Serialize (ref syncPosition);
								stream.Serialize (ref size);
								stream.Serialize (ref weight);

								GetComponent<NaturalResources> ().size = size;
								GetComponent<NaturalResources> ().weight = weight;
								transform.position = syncPosition;
			
						}
				} else {
			if (stream.isWriting) {
				st = GetComponent<HostFlag>().StartGame;
			
				stream.Serialize (ref st);
				
			} else {

				stream.Serialize (ref st);
				
				 GetComponent<HostFlag>().StartGame = st;

			}
				}
	}
}
