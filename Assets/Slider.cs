﻿using UnityEngine;
using System.Collections;

public class Slider : MonoBehaviour {

	private bool StartSlider = false;
	[SerializeField]
	Camera SlideCamera;
	float x;
	[SerializeField]
	float k;
	public Vector3 std;

	
	// Update is called once per frame
	void Update () 
	{
				if (Input.GetMouseButtonDown (0)) 
		    {
						RaycastHit hit = new RaycastHit ();
						Ray r = SlideCamera.ScreenPointToRay (Input.mousePosition);
					
				if(Physics.Raycast(r, out hit,1000))
				{
					if(hit.transform.tag == "Inv_obj")
					StartSlider = true;
					x = Input.mousePosition.x;
				//	std = transform.localPosition;
				}

			}

			if (Input.GetMouseButtonUp (0)) 
			{
				StartSlider = false;
			}

		if (Input.GetMouseButton (0)) 
		{
			if(StartSlider == true)
			{
				float delta = Input.mousePosition.x - x;
				if((transform.localPosition.x + delta*k) < 0 && (transform.localPosition.x + delta*k) > GetComponent<InventoryScript>().CountObj * (-340))
				//transform.localPosition = new Vector3(transform.localPosition.x + delta*k,0,0);
					std = new Vector3(transform.localPosition.x + delta*k,0,0);
			}
		}

		transform.localPosition = Vector3.MoveTowards (transform.localPosition, std, 3);
	}
	
	}

	