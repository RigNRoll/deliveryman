﻿using UnityEngine;
using System.Collections;

public class GameControll : MonoBehaviour {

	GameObject Inventory;
	[SerializeField]
	GameObject FullMapCamera;
 	public string NameButton;
	int count = 0;

	void Start()
	{
		Inventory = GameObject.Find ("Inventory");
	}
	void OnClick()
	{

		switch(NameButton)
		{
		case "CloseInv"	:
		{
							if (Inventory.GetComponent<InventoryScript> ().OnOFF) 
									Inventory.GetComponent<InventoryScript> ().OffInventar ();
								 else 
									Inventory.GetComponent<InventoryScript> ().OnInventar ();
		}
		
								break;

		case "Map" : 
		{
			count++;
			if(count%2==1)
				FullMapCamera.SetActive (true);
			else
				FullMapCamera.SetActive(false);
		} 
			break;

		case "DeleteObj" : 
			Inventory.GetComponent<InventoryScript>().Drop(transform.parent.transform);
			break;

		case "Sell" : 
			Inventory.GetComponent<InventoryScript>().Sell(); 
			break;
		case "Disconect" : 

			GameObject.Find("ServerControll").GetComponent<Server>().Disconected();
			break;
		case "Create" : 
		{
			GameObject.Find("ServerControll").GetComponent<Server>().CreateServer();

		}break;
		case "RefreshHost" : 
		{
			GameObject.Find("ServerControll").GetComponent<Server>().RefreshHost();
		}break;
		case "StartHost" : 
		{
			GameObject.Find("ServerControll").GetComponent<Server>().StartGame();
		}break;
		case "Refresh" : 
		{
			foreach(GameObject G in	GameObject.FindGameObjectsWithTag("Player"))
			{
				G.GetComponent<Car>().Refresh();
			}
		}
			break;
		case "BackToMainMenu" : 
		{
			Application.LoadLevel("MainMenu");
		} 
			break;
		}

	}
}
