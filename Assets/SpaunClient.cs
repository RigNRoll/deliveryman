﻿using UnityEngine;
using System.Collections;

public class SpaunClient : MonoBehaviour {


	void OnSerializeNetworkView (BitStream stream, NetworkMessageInfo info) {
		bool use = false;
		if (stream.isWriting) {
			use = GetComponent<RPlaces>().Use;

			stream.Serialize(ref use);
			
		} else {
			stream.Serialize(ref use);

			GetComponent<RPlaces>().Use = use;
		}
	}
}
